package com.meicai.data.pojo;

import com.meicai.data.core.DataContext;
import org.apache.commons.lang3.StringUtils;

/**
 * 每个操作记录
 */
public class Item implements Cloneable {

    public long id = 0;

    public String billId = StringUtils.EMPTY;

    public String cityId = StringUtils.EMPTY;

    public String warehouseId = StringUtils.EMPTY;

    public String storageArea = StringUtils.EMPTY;

    public String storageAreaType = StringUtils.EMPTY;

    public String loc = StringUtils.EMPTY;

    public String containerId = StringUtils.EMPTY;

    public String skuId = StringUtils.EMPTY;

    public String ssuId = StringUtils.EMPTY;

    public String packId = StringUtils.EMPTY;

    public String batchId = StringUtils.EMPTY;

    public double currentVal = 0;

    public int changeType = 0;

    public double changeVal = 0;

    public double changeValFormat = 0;

    public int operateTime = 0;

    public int transModule = 0;

    public int operateType = 0;

    public int c_t = 0;

    public int u_t = 0;

    public int realSeq = Integer.MAX_VALUE;

    public int latestContainerItemRealSeq = Integer.MAX_VALUE;

    //SKU批次库存量
    public double batchStockAmount = 0;

    //SKU批次被动盘盈数量
    public double batchOverageAmount = 0;


    public Item clone() {
        try {
            return (Item) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String toString() {
        String fields[] = {id + "", billId, cityId, warehouseId, storageArea, storageAreaType, loc, containerId,
                skuId, ssuId, packId, batchId, currentVal + "", changeType + "", changeVal + "", changeValFormat + "",
                batchStockAmount + "", batchOverageAmount + "", operateTime + "", transModule + "", operateType + "",
                c_t + "", u_t + "", realSeq + "", latestContainerItemRealSeq + ""};
        return StringUtils.join(fields, "\t");
    }

}
