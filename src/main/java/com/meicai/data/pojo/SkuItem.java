package com.meicai.data.pojo;

/**
 * sku基本属性
 */
public class SkuItem {
    public String cityId;
    public String warehouseId;
    public String siid;
    public String billId;

    public SkuItem(String cityId, String operateArea, String siid, String billId){
        this.cityId = cityId;
        this.warehouseId = operateArea;
        this.siid = siid;
        this.billId = billId;
    }

    public String toString(){
        return String.format("%s_%s_%s_%s", siid, cityId, warehouseId, billId);
    }
}
