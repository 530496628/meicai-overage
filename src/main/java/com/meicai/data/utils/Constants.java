package com.meicai.data.utils;

/**
 * 常量类
 *
 * @author LiuYsn
 */
public class Constants {

    //期初
    public final static String INITIAL = "100";

    //采购入库
    public final static String PURCHASE_STORED = "1";

    //扣称入库
    public final static String PURCHASE_DEDUCTION_STORED = "2";

    //赠品入库
    public final static String PURCHASE_FREE_STORED = "3";

    //调拨入库
    public final static String ALLOCATE_INSTOCK = "4";

    //调拨入暂存
    public final static String ALLOCATE_TEMPORARY_STORED = "5";

    //退货入暂存
    public final static String RETURN_GOODS_STORED = "6";

    //退货入暂存调整
    public final static String ADJUST_RETURN_GOODS_STORED = "62";

    //退货入暂存取消
    public final static String RETURN_GOODS_STORED_CANCEL = "63";

    //盘亏
    public final static String WAREHOUSE_CHECK_LOSS = "10";
    
    //盘盈
    public final static String WAREHOUSE_CHECK_PROFIT = "11";

    //报损
    public final static String WAREHOUSE_DAMAGED = "12";

    //退供应商
    public final static String RETREAT_SUPPLIER = "13";

    //调拨出库
    public final static String ALLOCATE_OUTSTOCK = "14";

    //发运
    public final static String WAREHOUSE_DISPATCH = "31";

    //分拣
//    public final static String SORTING_TASK = "31";

    //分拣任务取消
    public final static String SORTING_TASK_CANCEL = "32";

    //分拣包裹取消
    public final static String SORTING_PACKAGE_CANCEL = "33";

    //司机调量
    public final static String DRIVER_ADJUST = "41";

    //司机补货
    public final static String DRIVER_REPLENISHMENT = "42";

    //客户拒收
    public final static String CUSTOMER_REJECT_GOODS = "43";

    //订单直减
    public final static String REDUCE_ORDER_MONEY = "44";

    //司机确认
    public final static String DRIVER_CONFIRMATION = "45";

    //客户申请退货
    public final static String CUSTOMER_RETURN_GOODS = "46";

    //对冲直减
    public final static String HEDGE_REDUCE_MONEY = "47";

    //司机调量盘亏
    public final static String BALANCE_DRIVER_ADJUST = "48";

    //客户退货盘亏
    public final static String BALANCE_CUSTOMER_RETURN_GOODS = "49";

    //数量调整单入库
    public final static String ADJUST_INSTOCK_AMOUNT = "51";

    //数量调整单盘点
    public final static String BALANCE_ADJUST_INSTOCK_AMOUNT = "52";

    //数量调整单盘点调整
    public final static String RELOAD_ADJUST_INSTOCK_AMOUNT = "53";

    //数量调整单出库
    public final static String ADJUST_OUTSTOCK_AMOUNT = "54";

    //数量出库调整单盘点
    public final static String BALANCE_ADJUST_OUTSTOCK_AMOUNT = "55";

    //数量出库调整单盘点
    public final static String RELOAD_ADJUST_OUTSTOCK_AMOUNT = "56";

    //价格调量
    public final static String ADJUST_PRICE = "61";

    //针对库存量为0 金额不为0 平衡
    public final static String BALANCE_ZERO_STOCK = "19";

    //报损调整
    public final static String ADJUST_WAREHOUSE_DAMAGED = "121";

}
