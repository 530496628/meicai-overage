package com.meicai.data.utils;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 配置工具类
 * @author LiuYan
 *
 */
public class ConfigureUtil {

	private static final Properties props = new Properties();
	
	Map<String, String> configre = new ConcurrentHashMap<String, String>();
	
	static {
		try {
			props.load(ConfigureUtil.class.getResourceAsStream("/config.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static String getProperty(String key) {
		return props.getProperty(key);
	}
}
