package com.meicai.data.core;

import com.meicai.data.engine.DataTransform;
import com.meicai.data.pojo.Item;
import com.meicai.data.pojo.SkuItem;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.io.IOException;
import java.util.*;

/**
 * 数据上下文
 */
public class DataContext {
    private SkuItem sku;
    /**
     * 流水表
     */
    public List<Item> itemList = new ArrayList<>();
    /**
     * 最新的批次信息
     */
    private Map<String, Item> latestContainerItem = new HashMap<>();

    private DataTransform transform;


    public DataContext(SkuItem sku, DataTransform transform) {
        this.sku = sku;
        this.transform = transform;
    }


    public void addData(Item item) {
        if (null != item) { //出现null是为了让list添加一个空元素
            item.realSeq = itemList.size();
        }
        itemList.add(item);
    }

    /**
     * 对dataList进行预处理
     */
    public void preProcess() {
        this.loadItems(this.transform.fetchData());
    }


    public void registLatestContainerItem(Item item) {
        if(item == null) {
            return;
        } else {
            latestContainerItem.put(item.containerId,item);
        }
    }

    public void registPreItemRealSeq(Item item) {
        if(item == null) {
            return;
        } else {
            Item latestItem = latestContainerItem.get(item.containerId);
            if(latestItem != null) {
                item.latestContainerItemRealSeq = latestItem.realSeq;
            } else {
                item.latestContainerItemRealSeq = Integer.MAX_VALUE;
            }
        }
    }

    public Item getLatestContainerItem(Item item) {
        if(item == null || item.latestContainerItemRealSeq >= itemList.size()) {
            return new Item();
        } else {
            Item latestItem = itemList.get(item.latestContainerItemRealSeq);
            return latestItem;
        }
    }


    /**
     * 清空map
     */
    public void clear() {
        this.itemList.clear();
    }

    /**
     * 保存map到db
     */
    public void save() throws IOException, InterruptedException {
        this.transform.sinkData(this);
        clear();
    }


    /**
     * 加载数据
     *
     * @param lines
     */
    public void loadItems(Iterable<String> lines) {
        Item item;
        for (String line : lines) {
            String[] stepItems = StringUtils.split(line.trim(), this.transform.getSep());
            item = new Item();
            item.id = NumberUtils.isNumber(stepItems[0]) ? Long.parseLong(stepItems[0]) : 0;
            item.billId = stepItems[1];
            item.cityId = stepItems[2];
            item.warehouseId = stepItems[3];
            item.storageArea = stepItems[4];
            item.storageAreaType = stepItems[5];
            item.loc = stepItems[6];
            item.containerId = stepItems[7];
            item.skuId = stepItems[8];
            item.ssuId = stepItems[9];
            item.packId = stepItems[10];
            item.batchId = stepItems[11];
            item.currentVal = NumberUtils.isNumber(stepItems[12]) ? Double.parseDouble(stepItems[12]) : 0;
            item.changeType = NumberUtils.isNumber(stepItems[13]) ? Integer.parseInt(stepItems[13]) : 0;
            item.changeVal = NumberUtils.isNumber(stepItems[14]) ? Double.parseDouble(stepItems[14]) : 0;
            item.changeValFormat = NumberUtils.isNumber(stepItems[15]) ? Double.parseDouble(stepItems[15]) : 0;
            item.operateTime = NumberUtils.isNumber(stepItems[16]) ? Integer.parseInt(stepItems[16]) : 0;
            item.transModule = NumberUtils.isNumber(stepItems[17]) ? Integer.parseInt(stepItems[17]) : 0;
            item.operateType = NumberUtils.isNumber(stepItems[18]) ? Integer.parseInt(stepItems[18]) : 0;
            item.c_t = NumberUtils.isNumber(stepItems[19]) ? Integer.parseInt(stepItems[19]) : 0;
            item.u_t = NumberUtils.isNumber(stepItems[20]) ? Integer.parseInt(stepItems[20]) : 0;
            this.addData(item);
        }
    }
}
