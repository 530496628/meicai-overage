package com.meicai.data.core;

import com.meicai.data.engine.DataTransform;
import com.meicai.data.handler.ItemHandler;
import com.meicai.data.pojo.SkuItem;

/**
 * 跑si各类型数据的任务
 * Created by wangzejie on 15/11/23.
 */
public class Task implements Runnable {

    private SkuItem sku;
    public DataContext dataContext;

    public Task(SkuItem sku, DataTransform fetcher) {
        this.sku = sku;
        this.dataContext = new DataContext(sku, fetcher);
    }

    @Override
    public void run() {
        this.execute();
    }

    public void execute() {
        try {
            long start = System.currentTimeMillis();
            dataContext.preProcess();
            ItemHandler handler = new ItemHandler();
            handler.process(dataContext);
            dataContext.save();
            System.out.println(String.format("Task[%s] finished.Cost %ss", this.sku, (System.currentTimeMillis() - start) / 1000));
        }catch(Exception e){
            e.printStackTrace(System.out);
            System.out.println(String.format("Task[%s] error. %s", this.sku, e.getMessage()));
        }
    }
}
