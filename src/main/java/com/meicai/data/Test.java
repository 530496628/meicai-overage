package com.meicai.data;

import com.meicai.data.core.Task;
import com.meicai.data.engine.local.LocalFileTransform;
import com.meicai.data.pojo.SkuItem;

import java.io.FileNotFoundException;

/**
 * 本地单SKU单批次测试
 */
public class Test {
    public static void main(String args[]) throws FileNotFoundException {
        SkuItem sku = new SkuItem("1", "1", "1", "1");
        Thread task = new Thread(new Task(sku, new LocalFileTransform(sku, true)));
        task.start();
    }
}


