package com.meicai.data;

import com.meicai.data.engine.Engine;
import com.meicai.data.engine.distribute.MREngine;

/**
 * Created by wangzejie on 15/11/26.
 */
public class Main {
    public static void main(String[] args) throws Exception {
        Engine engine = new MREngine();
        engine.execute();
        //传参，默认为0，当手动设置时为1
        
        //增加方法，将此临时表数据更新到设置的（期初+当前时间）指定分区 overwrite into partition
        //增加方法，取此临时表数据和T表中标记为1合并 
        
        //判断异常，如果是异常默认标记为1 union T中标记为1且（城市，仓库，si不同）的数据
        //需要再跑一次从期初到目前的数据
    }
}
