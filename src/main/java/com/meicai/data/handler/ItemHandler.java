package com.meicai.data.handler;

import com.meicai.data.core.DataContext;
import com.meicai.data.pojo.Item;
import com.meicai.data.utils.Constants;

public class ItemHandler implements Cloneable {

    /**
     * 包含库间操作的被动盘盈
     * @param dataContext
     * @throws Exception
     */
    // TODO: 2016/4/15 目前仅处理库间进出动作，没有考虑调量单
    public void handle(DataContext dataContext, Item item) throws Exception {
        Item latestItem = dataContext.getLatestContainerItem(item);
        if(item.changeType == 1) {
            item.batchStockAmount = latestItem.batchStockAmount + item.changeValFormat;
        } else if(item.changeType == 2) {
            item.batchStockAmount = latestItem.batchStockAmount - item.changeValFormat;
            if(latestItem.batchStockAmount <= 0) {
                item.batchOverageAmount = item.changeValFormat;
            } else if(item.batchStockAmount < 0) {
                item.batchOverageAmount = -item.batchStockAmount;
            }
        }
        else if(item.changeType == 3 || item.changeType == 4) {
            item.batchStockAmount = latestItem.batchStockAmount;
        }

    }

    public void process(DataContext dataContext) throws Exception {
        for (int i = 0; i < dataContext.itemList.size(); i ++) {
            Item item = dataContext.itemList.get(i);
            dataContext.registPreItemRealSeq(item);
            handle(dataContext, item);
            dataContext.registLatestContainerItem(item);
        }
    }
}
