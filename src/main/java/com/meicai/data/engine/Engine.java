package com.meicai.data.engine;

/**
 * Created by wangzejie on 15/11/26.
 */
public interface Engine {
    void execute() throws Exception;
}
