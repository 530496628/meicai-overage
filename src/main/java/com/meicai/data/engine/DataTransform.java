package com.meicai.data.engine;

import com.meicai.data.core.DataContext;

import java.io.IOException;

/**
 *
 * 数据转换器，从哪抽取，放哪去
 * Created by wangzejie on 15/11/26.
 */
public abstract class DataTransform {

    protected boolean test;
    protected String sep = "\t";//分隔符

    public DataTransform(boolean test){
        this.test = test;
    }
    /**
     * 获得数据
     * @return
     */
    public abstract Iterable<String> fetchData();

    /**
     * 保存数据
     * @param dataContext
     * @return
     */
    public abstract boolean sinkData(DataContext dataContext) throws IOException, InterruptedException;

    public String getSep() {
        return sep;
    }

    public void setSep(String sep) {
        this.sep = sep;
    }

    public boolean isTest() {
        return test;
    }

    public void setTest(boolean test) {
        this.test = test;
    }
}
