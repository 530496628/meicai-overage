package com.meicai.data.engine.distribute;

import com.meicai.data.pojo.SkuItem;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Mapper
 */
public class SkuMapper extends Mapper<LongWritable, Text, SecondarySortApi.SkuOutputKey, Text>{
    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String items[] = value.toString().split("\t");
        SkuItem sku = new SkuItem(items[2], items[3], items[8], items[11]);
        long id = NumberUtils.isNumber(items[0]) ? Long.parseLong(items[0]) : 0;
        long operateTime = NumberUtils.isNumber(items[16]) ? Long.parseLong(items[16]) : 0;
        context.write(new SecondarySortApi.SkuOutputKey(sku, operateTime, id), value);
    }
}
