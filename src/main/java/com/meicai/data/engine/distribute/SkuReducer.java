package com.meicai.data.engine.distribute;

import com.meicai.data.core.Task;
import com.meicai.data.engine.DataTransform;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

import java.io.IOException;


/**
 * reducer, 处理各个skukey.
 */
public class SkuReducer extends Reducer<SecondarySortApi.SkuOutputKey, Text, NullWritable, Text> {

    @Override
    public void reduce(SecondarySortApi.SkuOutputKey key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        DataTransform dataTransform = new MRTransform(context, values, true);
        Task task = new Task(key.toSkuItem(), dataTransform);
        task.execute();
    }
}
